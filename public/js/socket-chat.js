var socket = io();
var params = new URLSearchParams(window.location.search);

const messageContainer = document.getElementById('message-container')
const messageForm = document.getElementById('send-container')
const messageInput = document.getElementById('message-input')
const userContainer = document.getElementById('user-container')
const userListContainer = document.getElementById('list-container')

const date = new Date();
var hours = date.getHours();
var minutes = date.getMinutes();
var ampm = hours >= 12 ? 'pm' : 'am';
hours = hours % 12;
hours = hours ? hours : 12; // the hour '0' should be '12'
minutes = minutes < 10 ? '0'+minutes : minutes;
var strTime = hours + ':' + minutes + ' ' + ampm;

//const name = prompt('What is your name?')
const name = prompt('Cual es tu nombre?');
appendMessage('Entraste a la sala');
appendMessageUser(name)
//appendMessageListUser(name)
socket.emit('new-user', name);

if (!name/* params.has("nombre") */) {
    window.location = "index.html";
    throw new Error("El nombre es necesario");
}

console.log("Nombre", name);

var usuario = {
    //nombre: params.get("nombre"),
    nombre: name,
};

socket.on("connect", function() {
    console.log("Conectado al servidor");

    socket.emit("entrarChat", usuario, function (resp) {
        console.log("Usuarios conectados", resp);
/*         var nombres = resp.map(function(data) {
            return data.nombre
        })
        //console.log("Lista 111111", JSON.stringify(nombres));
        //console.log("Lista 222222", nombres);
        appendMessageListUser(nombres);
        socket.emit('update', nombres); */
    });
});

socket.on("disconnect", function () {
    console.log("Perdimos conexion con el servidor");
});

socket.on("crearMensaje", function (mensaje) {
    console.log("servidor mensaje", mensaje);
});

socket.on("listaPersonas", function(personas) {
    console.log(personas);
});
/*--------------------------------------------------------*/
socket.on('user-connected', name => {
    appendMessage(`${name} se unio a la sala`)
})

socket.on('user-disconnected', name => {
    appendMessage(`${name} se fue de la sala`)
})

socket.on('chat-message', data => {
    appendMessage(`${data.name}: ${data.message} ` + strTime)
});

socket.on('update', function(personas) {
    console.log("Socket update")
    var listpersons = new Array();
    personas.map((persona) => {
        //console.log(persona.nombre);
        listpersons.push(persona.nombre);
        userListContainer.innerHTML = '';
    });

    listpersons.forEach(update);
    function update(value) {
        userListContainer.innerHTML += "<p><span>" + value + "</span></p>";
    }

    /*function update(array) {
        for (var i = 0; i < array.length; i++){
            userListContainer.innerHTML += "<p>" + array[i] + "</p>";
        }
    }
    update(listpersons); */
});

messageForm.addEventListener('submit', e => {
    e.preventDefault()
    const message = messageInput.value
    appendMessage(`Tu: ${message} ` + strTime)
    socket.emit('send-chat-message', message)
    messageInput.value = ''
})

function appendMessage(message) {
    const messageElement = document.createElement('div')
    messageElement.innerText = message
    messageContainer.append(messageElement)
}

function appendMessageUser(message) {
    const messageElement = document.createElement('div')
    messageElement.innerText = message
    userContainer.append(messageElement)
}

/* function appendMessageListUser(message) {
    message.forEach(myFunction);

    //const messageElement = document.createElement('div')
    //messageElement.innerText = message;
    //userListContainer.append(messageElement)

    function myFunction(value) {
        const messageElement = document.createElement('div')
        messageElement.innerText = value;
        userListContainer.append(messageElement)
    }
} */